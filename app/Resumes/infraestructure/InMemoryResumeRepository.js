const { ResumeRepository } = require('../domain/ResumeRepository');

class InMemoryResumeRepository extends ResumeRepository {
    constructor(resumes) {
        super();

        if (typeof resumes !== 'undefined') {
            this.resumes = resumes;
        } else {
            this.resumes = [
                {
                    _id: 1,
                    scss: `@import url('https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&display=swap');
@import url('https://fonts.googleapis.com/icon?family=Material+Icons');

font-family: 'Montserrat', sans-serif;
display: flex;
flex-direction: column;

.header-row {
  position: relative;
  min-height: 6cm;
  background-color: #4285f4;
  margin-bottom: 0.5cm;
  color: #ffffff;

  .details {
    width: 100%;
    padding: 0.5cm;

    .name {
      font-size: 2em;
      font-weight: bold;
      margin-bottom: 0.15em;
    }

    .address {
    }

    .note {
      font-weight: 300;
      font-size: 0.75em;
    }

    .phone {
      margin-bottom: 0.25em;
    }

    .phone, .email {
      a {
        color: #ffffff;
        text-decoration: none;
        display: flex;
        align-items: center;

        .material-icons {
          margin-right: 5px;
        }
      }
    }
  }

  .triform {
    width: 100%;
    height: 60px;
    background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI2NzQuMyIgaGVpZ2h0PSIxMzQuMyIgdmlld0JveD0iMCAwIDE3OC40IDM1LjUiIHZlcnNpb249IjEuMSIgaWQ9InN2ZzgiPjxnIGlkPSJsYXllcjEiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0yMCAtMTE2KSI+PHBhdGggZD0iTTIwIDE1MWgxNzh2LTM1eiIgaWQ9InRyaWFuZ2xlIiBmaWxsPSIjZmZmIiBmaWxsLW9wYWNpdHk9IjEiIHN0cm9rZT0ibm9uZSIgc3Ryb2tlLXdpZHRoPSIuMyIgc3Ryb2tlLWxpbmVjYXA9ImJ1dHQiIHN0cm9rZS1saW5lam9pbj0ibWl0ZXIiIHN0cm9rZS1vcGFjaXR5PSIxIi8+PC9nPjwvc3ZnPg==");
    background-repeat: no-repeat;
    background-size: cover;
    background-position-x: 3px;
    background-position-y: -70px;
  }

  .picture {
    position: absolute;
    bottom: 0;
    left: calc(50% - 45px);
    width: 120px;
    height: 120px;
    overflow: hidden;
    border-radius: 50%;
    background: #dedede;
    border: 3px solid #cccccc;
    -webkit-box-shadow: 0px 0px 8px 0px rgba(0, 0, 0, 0.35);
    -moz-box-shadow: 0px 0px 8px 0px rgba(0, 0, 0, 0.35);
    box-shadow: 0px 0px 8px 0px rgba(0, 0, 0, 0.35);

    img {
      width: 100%;
      height: 100%;
      padding: 3px 3px 0 3px;
      object-fit: contain;
      object-position: bottom;
    }
  }
}

.row.content {
  flex: 1 1 0;

  & > .column {
    padding: 0 20px;
  }
}

.data-block {
  .date {
    font-size: 0.8em;
    color: #999999;
  }

  .header {
    margin: 0 0 0.1cm 0;
    font-weight: bold;
    font-size: 1em;
    text-align: left;

    .subheader {
      font-size: 0.8em;
      font-weight: 400;

      &:before {
        content: '- ';
      }
    }
  }

  .place {
    margin: 0 0 0.15cm 0.15cm;
    font-size: 0.8em;

    &:before {
      content: '@ ';
    }
  }

  .text {
    font-size: 0.8em;

    .subtext {
      .header {
        margin: 0 0 5px 0;
      }

      .content {
        p {
          margin: 0 0 10px 0;
        }
      }

      &:last-of-type {
        .content {
          margin-bottom: 15px;
        }
      }
    }
  }

  .data-body {
    font-size: 0.8em;
  }

  .skills {
    margin-top: 0.15cm;
    text-align: left;

    .chip {
      font-size: 0.9em;
      color: #999999;
      display: inline-block;
      border: 1px solid #cecece;
      border-radius: 16px;
      padding: 2px 10px;
      margin: 0 4px 8px 0;
    }
  }

  &.margin {
    margin-bottom: 15px;

    .subheader {
      color: #000000;
      font-size: 0.8em;
      font-weight: bold;
      margin-bottom: 5px;

      a {
        color: inherit;
        text-decoration: none;
        display: flex;
        align-items: center;

        .material-icons {
          margin-right: 5px;
        }

        .link {
          margin-left: 5px;
          font-weight: 400;

          &:before {
            content: '- ';
          }
        }
      }
    }

    .data-body {
      margin-bottom: 15px;
    }
  }

  .separator {
    width: 45%;
    height: 1px;
    background: #efefef;
    margin: 0.3cm 0 0.4cm 0;
  }

  &:last-of-type {
    .separator {
      display: none;
    }
  }
}

.footer {
  padding: 25px 15px;
  font-size: 0.7em;
  color: #999999;

  .brand {
    padding-left: 3px;

    a {
      text-decoration: none;
      color: #555555;
    }
  }
}`,
                    html: `<div class="header-row column">
    <div class="row flex details">
        <div class="column flex">
            <span class="name">Asier Paz Martínez</span>
            <span><strong>Self-taught</strong> Full Stack Developer</span>
            <span class="address">Cartagena, Spain</span>
            <span class="note">(planning on moving to Barcelona)</span>
        </div>
        <div class="column flex align-items-flex-end">
            <span class="phone"><a href="tel:+34650768178" target="_blank"><i class="material-icons">link</i> (+34) 650 768 178</a></span>
            <span class="email"><a href="mailto:asier@lifestamp.org?subject=Hello Asier&body=I'm hiring you!" target="_blank"><i class="material-icons">link</i> asier@lifestamp.org</a></span>
        </div>
    </div>
    <div class="triform"></div>
    <div class="picture">
        <img src="https://lifestamp.org/images/Asier.png" />
    </div>
</div>
<div class="content row justify-content-center align-items-flex-start">
    <div class="column flex-60">
        <div class="data-block">
            <div class="date">12/2016 - Present</div>
            <div class="header">
                Full-Stack Developer
            </div>
            <div class="place">Promociones Sierra Minera, La Unión (Spain)</div>
            <div class="text">
                <p>I've been the main developer since 2016 and I've made a full CMS and CRM from scratch, a REST API, and a bunch of minor projects.</p>
                <p>I've worked with a wide range of technologies, including PHP with Laravel, NodeJS and WebSockets with PusherJS among others.</p>
            </div>
            <div class="skills">
                <span class="chip">Laravel Framework 5</span>
                <span class="chip">MySQL</span>
                <span class="chip">Javascript</span>
                <span class="chip">AngularJS</span>
                <span class="chip">NodeJS</span>
                <span class="chip">SCSS</span>
                <span class="chip">PusherJS</span>
                <span class="chip">Webpack</span>
                <span class="chip">Gulp</span>
                <span class="chip">NPM</span>
                <span class="chip">Composer</span>
                <span class="chip">NGINX</span>
                <span class="chip">Docker</span>
                <span class="chip">Kubernetes</span>
                <span class="chip">OAuth 2.0</span>
                <span class="chip">REST API</span>
                <span class="chip">Blue-Green Deployment</span>
                <span class="chip">AngularJS Material</span>
            </div>
            <div class="separator"></div>
        </div>
        <div class="data-block">
            <div class="date">03/2015 - 06/2016</div>
            <div class="header">
                Full-Stack Developer
                <span class="subheader">Freelance</span>
            </div>
            <div class="place">Everywhere inside Spain</div>
            <div class="text">
                <p>I made a bunch of small jobs mostly for local businesses, but 
                here are the ones worth to mention:</p>
                <div class="subtext">
                    <div class="header">Cronomur</div>
                    <div class="content">
                        <p>
                            Development of a web page for a local sport events timekeeper business
                        that handles participant inscriptions to events and payments based on Redsys's POS.
                        </p>
                        <p>
                            It also can export participant data in XML format and has a feature
                        that lets the user see real
                        time ranking of participants of an ongoing event.
                        </p>
                    </div>
                </div>
                <div class="subtext">
                    <div class="header">Cronomur</div>
                    <div class="content">
                        <p>Development of a C# application with XAML to connect and read data out of RFID tags using the SAAT-F520 reader to time participants of sport events at the finish line.</p>
                    </div>
                </div>
            </div>
            <div class="skills">
                <span class="chip">Scala</span>
                <span class="chip">Play! Framework 2</span>
                <span class="chip">AngularJS</span>
                <span class="chip">MySQL</span>
                <span class="chip">Dart</span>
                <span class="chip">WebSockets</span>
                <span class="chip">Javascript</span>
                <span class="chip">JQuery</span>
                <span class="chip">PostgreSQL</span>
                <span class="chip">Java EE</span>
                <span class="chip">Java SE</span>
            </div>
            <div class="separator"></div>
        </div>
    </div>
    <div class="column flex-40">
        <div class="data-block margin">
            <div class="header">
                Top Skills
            </div>
            <div class="skills">
                <span class="chip">NodeJS</span>
                <span class="chip">Javascript</span>
                <span class="chip">TypeScript</span>
                <span class="chip">Java</span>
                <span class="chip">PHP</span>
                <span class="chip">React</span>
                <span class="chip">Redux</span>
                <span class="chip">Angular</span>
                <span class="chip">REST APIs</span>
                <span class="chip">Agile</span>
                <span class="chip">Scraping</span>
                <span class="chip">DDD</span>
                <span class="chip">Arquitectura Hexagonal</span>
                <span class="chip">TDD</span>
                <span class="chip">SOLID</span>
                <span class="chip">MongoDB</span>
                <span class="chip">Docker</span>
                <span class="chip">Kubernetes</span>
                <span class="chip">Linux</span>
                <span class="chip">Microservices</span>
                <span class="chip">RabbitMQ</span>
                <span class="chip">MySQL</span>
                <span class="chip">WebSockets</span>
            </div>
        </div>
        <div class="data-block margin">
            <div class="header">
                Side Projects
            </div>
            <div class="subheader link">
                <a href="https://lifestamp.org" target="_blank">
                    <i class="material-icons">link</i>
                    Lifestamp
                    <span class="link">https://lifestamp.org</span>
                </a>
            </div>
            <div class="data-body">
                Currently I am working in a platform for users to have their professional
                career in one place, providing resume and portfolio visual editors, a template
                system and a future marketplace for designers.
            </div>
            <div class="skills">
                <span class="chip">ExpressJs</span>
                <span class="chip">React</span>
                <span class="chip">Redux</span>
                <span class="chip">MongoDB</span>
                <span class="chip">Mongoose</span>
                <span class="chip">Material Web Components</span>
            </div>
        </div>
        <div class="data-block margin">
            <div class="header">
                Current interests / Active learning
            </div>
            <div class="data-body">
                I am constantly learning new things and feeding my curiosity.
                Currently I am interested in the following things:
            </div>
            <div class="skills">
                <span class="chip">Machine Learning</span>
                <span class="chip">Golang</span>
                <span class="chip">Master 3D Modeling</span>
                <span class="chip">Unreal Engine</span>
            </div>
        </div>
    </div>
</div>
<div class="footer row justify-content-center align-items-center">
    Proudly powered by <span class="brand"><a href="https://lifestamp.org" target="_blank">Lifestamp.org</a></span>
</div>`,
                    margins: {
                        paddingTop: '0',
                        paddingLeft: '0',
                        paddingBottom: '0',
                        paddingRight: '0'
                    }
                }
            ];
        }
    }

    find(id) {
        return new Promise((resolve, reject) => {
            const resume = this.resumes.find(x => x._id == id);
           if (typeof resume !== 'undefined') {
               resolve(resume);
           } else {
               reject('not found');
           }
        });
    }

    save(id, resume) {
        return new Promise((resolve, reject) => {
            let resumeFromDb = this.resumes.find(x => x._id == id);
            if (typeof resume !== 'undefined') {
                resumeFromDb = Object.assign(resumeFromDb, resume);
                this.resumes[this.resumes.findIndex(x => x._id == id)] = resumeFromDb;
                resolve(resumeFromDb);
            } else {
                reject('not found');
            }
        });
    }
}

module.exports = { InMemoryResumeRepository };