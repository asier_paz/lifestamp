const { ResumeRepository } = require('../domain/ResumeRepository');
const mongoose = require('mongoose');
const Resume = require('../../../models/Resume');

class MongoResumeRepository extends ResumeRepository {
    find(id) {
        return new Promise((resolve, reject) => {
            Resume.findById(id, (err, resume) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(resume);
                }
            });
        });
    }

    save(id, resume) {
        return new Promise((resolve, reject) => {
            Resume.findByIdAndUpdate(id, resume, (err, resume) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(resume);
                }
            });
        });
    }
}

module.exports = { MongoResumeRepository };