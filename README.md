# Description
Lifestamp is a place where any user can register and save his or her resumes and portfolios
forever free. Additionally, the user can edit his or her resumes or portfolios
with the built-in Resume Editor.

The main focus of the project is to give the average user the power to be able to
have his or her unique resume and/or portfolio and differentiate himself or herself
from other people.

__Note: The project is very new. There's only a few features of the resume editor.__

# Running it
First you need to install the dependencies using NPM in the root directory
and in the client directory.

In the root directory run the following command:
    
    npm install
    
Now in the __client__ directory run the following command:
    
    npm install
    
For the __client__ directory, if you want to be able to correctly run the
development server you need to set the environment variable __SASS_PATH__

__For Linux:__

    export SASS_PATH=./node_modules
    
__For Windows:__

    SET SASS_PATH=.\node_modules
    
Now copy the `.env.example` file to `.env`:

    cp .env.example .env
    
Finally, you only need to run the following command at the root directory:

    npm start
    
that would start the backend server. Now we need to run th frontend development server. For
that you need to run the following command at the client directory:

    npm run start
    
# Deployment
For the deployment you need to run the following command at the client
directory:

    npm run build
    
The last command should have minify and compress the frontend code and
moved the dist files to the __public__ directory.

Lastly, you need to start the backend server with the following command at the
root directory:

    npm start
    
__That's all for now folks. Remember this project is very new and I am
trying to make a proof of concept first__