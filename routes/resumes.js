const express = require('express');
const router = express.Router();
const ResumesController = require('../controllers/resumes.controller');

// Todo: Create a middleware to ensure the resources are owned by the user making the request

/* GET Find the given resume. */
router.get('/:id', ResumesController.find);

/* POST Save the given resume */
router.post('/:id', ResumesController.save);

module.exports = router;
