var express = require('express');
var router = express.Router();
var sass = require('node-sass');

/* POST transform SASS to CSS */
router.post('/sass/compile', function(req, res, next) {
  sass.render({
    data: req.body.sass,
    outputStyle: 'compressed'
  }, function(err, result) {
    if (err) {
      res.status(400)
          .json({
            status: 'error',
            error: {
              message: err.message,
              formatted: err.formatted,
              line: err.line,
              column: err.column
            }
          });
    } else {
      res.json({
        status: 'ok',
        css: result.css.toString('utf8')
      });
    }
  });
});

module.exports = router;
