const mongoose = require('mongoose');

const Schema = mongoose.Schema({
    // Todo: user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    // Todo: template: {type: mongoose.Schema.Types.ObjectId, ref: 'ResumeTemplate'},

    scss: {type: String, required: true, default: ''},
    html: {type: String, required: true, default: ''},
})

const Resume = mongoose.model('Resume', Schema);

module.exports = Resume;