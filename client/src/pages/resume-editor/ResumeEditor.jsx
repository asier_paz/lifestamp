import React, {Component} from 'react';
import './ResumeEditor.scss';
import {extend, debounce} from 'lodash-es';
import AceEditor from "react-ace";
import "ace-builds/src-noconflict/mode-html";
import "ace-builds/src-noconflict/mode-scss";
import "ace-builds/src-noconflict/theme-github";
import "ace-builds/src-min-noconflict/ext-searchbox";
import "ace-builds/src-min-noconflict/ext-language_tools";
import Button from "@material/react-button";
import TextField, {HelperText, Input} from "@material/react-text-field";
import MaterialIcon from "@material/react-material-icon";
import Select, {Option} from '@material/react-select';
import {Snackbar} from '@material/react-snackbar';
import TemplateSelectionDialog from "./template-selection-dialog/TemplateSelectionDialog";
import InitialFetchingDialog from "./initial-fetching-dialog/InitialFetchingDialog";

export default class ResumeEditor extends Component {
    state = {};
    pageStyleElement = document.createElement("style");
    documentTypes = [
        { name: "DIN A3 (Portrait)", cssClass: "A3" },
        { name: "DIN A3 (Landscape)", cssClass: "A3 landscape" },
        { name: "DIN A4 (Portrait)", cssClass: "A4" },
        { name: "DIN A4 (Landscape)", cssClass: "A4 landscape" },
        { name: "DIN A5 (Portrait)", cssClass: "A5" },
        { name: "DIN A5 (Landscape)", cssClass: "A5 landscape" }
    ];

    constructor(props) {
        super(props);

        console.log("Resume id: %o", props.match.params.cvid);

        // Set initial state here
        this.state = {
            resumeId: props.match.params.cvid,
            resume: {
                margins: {
                    paddingTop: '2.5cm',
                    paddingLeft: '2.5cm',
                    paddingBottom: '2.5cm',
                    paddingRight: '2.5cm'
                }
            },
            editorPanelShowing: false,
            editorPanelFullscreen: false,
            panelModeShowing: {
                document: false,
                html: false,
                sass: false
            },
            documentType: "2",
            documentTemplate: {
                name: 'Una columna',
                docHtml: '',
                docSass: `.page {
  $text-color: red;

  .special-text {
    color: $text-color;
  }
}`,
                docCss: ''
            },
            docCss: '',
            dialogs: {
                initialFetching: false,
                templateSelection: false
            }
        };
    }

    async componentDidMount() {
        document.head.appendChild(this.pageStyleElement);

        const resume = await (await fetch(`/resume/${this.state.resumeId}`)).json();
        this.setState({ resume }, this.compileSass);
    }

    /**
     * Open the sidenav panel in the given mode
     */
    openPanelMode = mode => {
        switch (mode.toLowerCase()) {
            case 'document':
                this.setState({
                    editorPanelShowing: true,
                    panelModeShowing: {
                        document: true
                    }
                });
                break;
            case 'html':
                this.setState({
                    editorPanelShowing: true,
                    panelModeShowing: {
                        html: true
                    }
                });
                break;
            case 'sass':
                this.setState({
                    editorPanelShowing: true,
                    panelModeShowing: {
                        sass: true
                    }
                });
                break;
            default:
                break;
        }
    };

    /**
     * Closes the left sidenav
     */
    closeLeftSidenav = () => {
        this.setState({
            editorPanelShowing: false,
            panelModeShowing: {
                document: false,
                html: false,
                sass: false
            }
        });
    };

    /**
     * Open the sidenav panel in the mode selection mode
     */
    showPanelModeSelection = () => {
        this.setState({
            editorPanelShowing: true,
            panelModeShowing: {
                document: false,
                html: false,
                sass: false
            }
        });
    };

    /**
     * Returns truthy if the mode selection is visible
     */
    isPanelModeSelectionVisible = () =>
        !this.state.panelModeShowing.document && !this.state.panelModeShowing.html && !this.state.panelModeShowing.sass;

    onSelectDocumentType = (index, item) =>
        this.setState({ documentType: item.getAttribute('data-value') });

    // Todo: Complete this feature
    templateSelectionDialogClosed = (result, payload) => {
        this.setState({ dialogs: extend({...this.state.dialogs}, {templateSelection: false}), documentTemplate: payload, docHtml: payload.docHtml, docSass: payload.docSass }, () => {
            this.compileSass().then(css => {
                console.log("Sass compiled to: %o", css);
                this.setState({ docCss: css });
            });

            console.log("Template selection dialog closed: %o | %o", result, payload);
        });
    };

    // Update the doc sass code in the state and compile it
    htmlEditorValueChange = (html) => {
        const resume = this.state.resume;
        resume.html = html;
        this.setState({ resume });
    };

    /**
     * Compiles the current document sass code to css
     * @return Promise
     */
    compileSass = async () => {
        const sass = this.state.resume.scss.trim().indexOf('.page') !== 0 ? `.page { ${this.state.resume.scss} }` : this.state.resume.scss;
        return new Promise((resolve, reject) => {
            fetch('/sass/compile', {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ sass })
            }).then(async (response) => {
                if (response.status !== 200) {
                    console.log('Looks like there was a problem. Status Code: ' +
                        response.status);
                    const body = await response.json();
                    console.log(body);
                    reject(body);
                    return;
                }

                // Examine the text in the response
                const data = await response.json();
                this.setState({ docCss: data.css }, this.updateCss);
                resolve(data);
            }).catch(err => {
                console.log('Fetch Error :-S', err);
                reject(err);
            });
        });
    };

    updateCss = () => {
        this.pageStyleElement.innerHTML = this.state.docCss;
    };

    // Update the doc sass code in the state and compile it
    sassEditorValueChange = (scss) => {
        const resume = this.state.resume;
        resume.scss = scss;
        this.setState({ resume }, this.compileSass);
    };

    updateDocumentMargins = (side, value) => {
        let margins = { ...this.state.resume.margins };
        switch (side) {
            case 'top':
                margins.paddingTop = value;
                break;
            case 'left':
                margins.paddingLeft = value;
                break;
            case 'bottom':
                margins.paddingBottom = value;
                break;
            case 'right':
                margins.paddingRight = value;
                break;
        }

        const resume = { ...this.state.resume, ['margins']: margins };
        this.setState({ resume });
    };

    /**
     * Saves the current resume state
     */
    saveDocument = () => {
        fetch(`/resume/${this.state.resumeId}`, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state.resume)
        }).then(async (response) => {
            if (response.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' +
                    response.status);
                console.log(await response.json());
                return;
            }

            // The resume was saved ok.
            this.setState({ saved: true });
        }).catch(err => {
            console.log('Fetch Error :-S', err);
        });
    };

    render() {
        return (
            <div className={`resume-editor ${this.state.editorPanelShowing ? 'side-panel-showing' : ''} ${this.state.panelModeShowing.document ? 'side-panel-mode-document-showing' : ''} ${this.state.panelModeShowing.html ? 'side-panel-mode-html-showing' : ''} ${this.state.panelModeShowing.sass ? 'side-panel-mode-sass-showing' : ''} ${this.isPanelModeSelectionVisible() ? 'side-panel-mode-selection-showing' : ''}`}>
                <section className={`side-panel ${this.state.editorPanelFullscreen ? 'side-panel-fullscreen' : ''}`}>
                    {/* Mode selection menu */}
                    <div className={`panel-mode-selection-wrapper`}>
                        <div className={`close-wrapper`}>
                            <button className={`btn-close`} onClick={this.closeLeftSidenav}>
                                <MaterialIcon icon="close" />
                            </button>
                        </div>
                        <button className={`panel-mode-button`} onClick={() => this.openPanelMode('document')}>
                            <MaterialIcon icon="description" />
                            <span className="panel-mode-label">Configuración del documento</span>
                        </button>
                        <button className={`panel-mode-button`} onClick={() => this.openPanelMode('html')}>
                            <img src='/ic/HTML5_1Color_Black.svg'  alt='HTML5 Logo'/>
                            <span className="panel-mode-label">Editor de HTML</span>
                        </button>
                        <button className={`panel-mode-button`} onClick={() => this.openPanelMode('sass')}>
                            <img src='/ic/Sass_Logo_Black.png' alt='SASS Logo' />
                            <span className="panel-mode-label">Editor de SCSS</span>
                        </button>
                    </div>

                    {/* Document Mode */}
                    <div className={`panel-mode-wrapper panel-mode-document`}>
                        <div className="back-to-selection-mode-wrapper">
                            <button className="btn-back-to-selection-mode" onClick={this.showPanelModeSelection}>
                                <MaterialIcon icon="arrow_back" />
                            </button>
                        </div>
                        <div className="mode-title">Configuración del documento</div>
                        <div className="form-wrapper">
                            <div className="selected-template-placeholder">
                                <strong>Plantilla seleccionada:</strong>
                            </div>
                            <Button className="select-template-btn" outlined onClick={() => this.setState({ dialogs: extend({ ...this.state.dialogs }, { templateSelection: true }) })}>Una columna</Button>

                            <Select
                                enhanced
                                label='Tipo de documento'
                                value={this.state.documentType}
                                onEnhancedChange={this.onSelectDocumentType}
                                className="default-input"
                            >
                                {
                                    this.documentTypes.map((op, idx) => (
                                        <Option key={'doc-type-' + idx} value={idx}>{ op.name }</Option>
                                    ))
                                }
                            </Select>
                            <section className="doc-margins">
                                <h4>Márgenes</h4>
                                <div className="row wrap quad-wrapper">
                                    <div className="input-wrapper row flex-50">
                                        <TextField className="flex" label='Arriba'>
                                            <Input value={this.state.resume.margins.paddingTop} onChange={ e => this.updateDocumentMargins('top', e.currentTarget.value) } />
                                        </TextField>
                                    </div>
                                    <div className="input-wrapper row flex-50">
                                        <TextField className="flex" label='Izquierda'>
                                            <Input value={this.state.resume.margins.paddingLeft} onChange={ e => this.updateDocumentMargins('left', e.currentTarget.value) } />
                                        </TextField>
                                    </div>
                                    <div className="input-wrapper row flex-50">
                                        <TextField className="flex" label='Abajo'>
                                            <Input value={this.state.resume.margins.paddingBottom} onChange={ e => this.updateDocumentMargins('bottom', e.currentTarget.value) } />
                                        </TextField>
                                    </div>
                                    <div className="input-wrapper row flex-50">
                                        <TextField className="flex" label='Derecha'>
                                            <Input value={this.state.resume.margins.paddingRight} onChange={ e => this.updateDocumentMargins('right', e.currentTarget.value) } />
                                        </TextField>
                                    </div>
                                </div>
                            </section>

                            <Button raised className="save-btn row align-items-center justify-content-center" onClick={this.saveDocument}>
                                <MaterialIcon icon="save" /> <span className="flex">Guardar</span>
                            </Button>
                        </div>
                    </div>

                    {/* HTML Mode */}
                    <div className={`panel-mode-wrapper panel-mode-html`}>
                        <div className="back-to-selection-mode-wrapper">
                            <button className="btn-back-to-selection-mode" onClick={this.showPanelModeSelection}>
                                <MaterialIcon icon="arrow_back" />
                            </button>
                            <span className="flex"></span>
                            <button className="btn-toggle-expand-mode" onClick={() => this.setState({ editorPanelFullscreen: !this.state.editorPanelFullscreen })}>
                                <MaterialIcon icon={this.state.editorPanelFullscreen ? 'fullscreen_exit' : 'fullscreen'} />
                            </button>
                        </div>
                        <div className="mode-title">HTML</div>
                        <div className="form-wrapper">
                            <AceEditor
                                placeholder="// Write your HTML here"
                                mode="html"
                                theme="github"
                                name="html-editor"
                                onChange={debounce(this.htmlEditorValueChange, 150)}
                                fontSize={14}
                                showPrintMargin={true}
                                showGutter={true}
                                highlightActiveLine={true}
                                value={this.state.resume.html}
                                width="100%"
                                height="100%"
                                setOptions={{
                                    enableBasicAutocompletion: false,
                                    enableLiveAutocompletion: true,
                                    enableSnippets: false,
                                    showLineNumbers: true,
                                    tabSize: 4,
                                }}/>
                        </div>
                    </div>

                    {/* SASS Mode */}
                    <div className={`panel-mode-wrapper panel-mode-sass`}>
                        <div className="back-to-selection-mode-wrapper">
                            <button className="btn-back-to-selection-mode" onClick={this.showPanelModeSelection}>
                                <MaterialIcon icon="arrow_back" />
                            </button>
                            <span className="flex"></span>
                            <button className="btn-toggle-expand-mode" onClick={() => this.setState({ editorPanelFullscreen: !this.state.editorPanelFullscreen })}>
                                <MaterialIcon icon={this.state.editorPanelFullscreen ? 'fullscreen_exit' : 'fullscreen'} />
                            </button>
                        </div>
                        <div className="mode-title">SCSS</div>
                        <div className="form-wrapper">
                            <AceEditor
                                placeholder="// Write your SCSS here"
                                mode="scss"
                                theme="github"
                                name="scss-editor"
                                onChange={debounce(this.sassEditorValueChange, 950)}
                                fontSize={14}
                                showPrintMargin={true}
                                showGutter={true}
                                highlightActiveLine={true}
                                value={this.state.resume.scss}
                                width="100%"
                                height="100%"
                                setOptions={{
                                    enableBasicAutocompletion: false,
                                    enableLiveAutocompletion: true,
                                    enableSnippets: false,
                                    showLineNumbers: true,
                                    tabSize: 4,
                                }}/>
                        </div>
                    </div>

                    {/* This should be always at the end */}
                    <span className="bottom-lifestamp-hint">Lifestamp</span>
                </section>

                {/* Editor space */}
                <section className="editor column justify-content-center align-items-center">
                    {/* Todo: The editor element should be the one using the dangerouslySetInnerHTML. That way we can do multi-page */}
                    <div
                        className={`page ${this.documentTypes[this.state.documentType].cssClass}`}
                        style={this.state.resume.margins}
                        dangerouslySetInnerHTML={{ __html: this.state.resume.html }}
                    ></div>

                    <div style={{display: "none"}}>
                        <div>
                            <Button className="test-button" onClick={() => this.setState({ isDialogOpen: true })}>Text</Button>
                            <Button raised className="test-button">Raised</Button>
                            <Button outlined className="test-button">Outlined</Button>
                        </div>
                        <div>
                            <Button className="test-button warn">Text</Button>
                            <Button raised className="test-button warn">Raised</Button>
                            <Button outlined className="test-button warn">Outlined</Button>
                        </div>
                        <div>
                            <Button className="test-button danger">Text</Button>
                            <Button raised className="test-button danger">Raised</Button>
                            <Button outlined className="test-button danger">Outlined</Button>
                        </div>
                        <div>
                            <Button disabled className="test-button">Text</Button>
                            <Button disabled raised className="test-button">Raised</Button>
                            <Button disabled outlined className="test-button">Outlined</Button>
                        </div>
                        <div>
                            <TextField
                                label='Dog'
                                helperText={<HelperText>Help Me!</HelperText>}
                                onTrailingIconSelect={() => this.setState({value: ''})}
                                trailingIcon={<MaterialIcon role="button" icon="delete"/>}
                            ><Input
                                disabled
                                value={this.state.value}
                                onChange={(e) => this.setState({value: e.currentTarget.value})} />
                            </TextField>
                            <TextField
                                label='Dog'
                                helperText={<HelperText>Help Me!</HelperText>}
                                onTrailingIconSelect={() => this.setState({value: ''})}
                                trailingIcon={<MaterialIcon role="button" icon="delete"/>}
                                outlined
                            ><Input
                                disabled
                                value={this.state.value}
                                onChange={(e) => this.setState({value: e.currentTarget.value})} />
                            </TextField>
                        </div>
                        <div>
                            <Select
                                disabled
                                enhanced
                                label='Choose Dog'
                                value={this.state.value}
                                onEnhancedChange={this.onEnhancedChange}
                            >
                                <Option value='pomsky'>Pomsky</Option>
                                <Option value='goldenDoodle'>Golden Doodle</Option>
                            </Select>
                        </div>
                    </div>
                </section>

                {/* Dialogs */}
                <InitialFetchingDialog open={this.state.dialogs.initialFetching} /> {/* Todo: Circle back to this */}
                <TemplateSelectionDialog open={this.state.dialogs.templateSelection} onClose={this.templateSelectionDialogClosed} />

                { this.state.saved ? <Snackbar message="Currículum vitae guardado correctamente" actionText="cerrar" onClose={() => this.setState({ saved: false })} /> : '' }
            </div>
        );
    }
}
