import React from 'react';
import './TemplateSelectionDialog.scss';
import Dialog, {DialogButton, DialogContent, DialogFooter} from "@material/react-dialog";
import List, {ListItem, ListItemText} from '@material/react-list';

function TemplateSelectionDialog(props) {
    const { onClose, selectedValue, open } = props;

    // Todo: Fetch available + owned templates
    const documentTemplates = [
        {
            name: 'Una columna',
            docHtml: '',
            docSass: '',
            docCss: ''
        },
        {
            name: 'Dos columnas',
            docHtml: '',
            docSass: '',
            docCss: ''
        },
        {
            name: 'Tres columnas',
            docHtml: '',
            docSass: `
$text-color: red;

.special-text {
    span {
        color: $text-color;
    }
}
            `,
            docCss: ''
        },
    ];

    const handleClose = (action) => {
        onClose(action);
    };

    const handleItemClick = (action, payload) => {
        onClose(action, payload);
    };

    return (
        <Dialog
            open={open}>
            <DialogContent>
                <List
                    singleSelection
                    handleSelect={ (selectedIndex) => handleItemClick('template-selected', documentTemplates[selectedIndex]) }>
                    {documentTemplates.map((x, idx) => (
                        <ListItem key={idx}>
                            <ListItemText primaryText={x.name} />
                        </ListItem>
                    ))}
                </List>
            </DialogContent>
            <DialogFooter>
                <DialogButton onClick={() => handleClose('dismiss')}>Cancel</DialogButton>
            </DialogFooter>
        </Dialog>
    );
}

export default TemplateSelectionDialog;