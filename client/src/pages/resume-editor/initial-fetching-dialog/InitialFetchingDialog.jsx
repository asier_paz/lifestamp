import React from 'react';
import './InitialFetchingDialog.scss';
import Dialog, {DialogContent} from "@material/react-dialog";

// Todo: Complete the dialog
function InitialFetchingDialog(props) {
    const { onClose, selectedValue, open } = props;

    return (
        <Dialog open={open}>
            <DialogContent>
                TODO
            </DialogContent>
        </Dialog>
    );
}

export default InitialFetchingDialog;