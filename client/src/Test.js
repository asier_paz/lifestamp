import React from 'react';
import $ from "jquery";
import logo from './logo.svg';
import './App.css';

function login() {
  $.ajax(
      {
        type: "POST",
        url: '/users/login',
        data: {
          email: $("#email").val(),
          password: $('#password').val()
        },
        success: () => {
            return {
                message: 'OKKK!'
            }
        }
      }
  )

}

function getUsers() {
    $.ajax(
        {
            type: "GET",
            url: '/users/',
            success: (result) => {
                console.log(result);
                return {
                    message: result
                }
            }
        }
    )

}

function Test() {
  return (
    <div className="Test">
      <label>email: </label><input type="text" id="email"/>
      <label>password: </label><input type="password" id="password"/>
      <button onClick={login}>send</button>
        <button onClick={getUsers}>obtain users</button>
    </div>
  );
}

export default Test;
