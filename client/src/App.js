import React, {Component} from 'react';
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';
import './App.scss';
import MyResumes from "./pages/my-resumes/MyResumes";
import ResumeEditor from "./pages/resume-editor/ResumeEditor";

// Testodipresto home
const Home = () => (
    <div>
        <h1>Home!</h1>
    </div>
);

export default class App extends Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route path='/' component={Home} exact/>
                    <Route path='/my-resumes' component={MyResumes}/>
                    <Route path='/resume/:cvid/edit' exact component={ResumeEditor}/>
                </Switch>
            </Router>
        );
    }
}
