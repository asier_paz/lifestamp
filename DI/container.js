var { MongoResumeRepository } = require('../app/Resumes/infraestructure/MongoResumeRepository');

const Container = {
    ResumeRepository: new MongoResumeRepository()
};

module.exports = Container;