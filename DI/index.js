
var Container = {};

if (process.env.APP_ENV === 'mock') {
    Container = require('./mock_container');
} else {
    Container = require('./container');
}

module.exports = Container;