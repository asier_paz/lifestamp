var { InMemoryResumeRepository } = require('../app/Resumes/infraestructure/InMemoryResumeRepository');

const Container = {
    ResumeRepository: new InMemoryResumeRepository()
};

module.exports = Container;