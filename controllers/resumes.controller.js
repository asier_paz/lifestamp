var { ResumeRepository } = require('../DI');

const ResumesController = {};

/**
 * Returns the resume with the given id
 * @param req
 * @param res
 */
ResumesController.find = (req, res) => {
    ResumeRepository.find(req.params.id).then(resume => {
        res.json(resume);
    }).catch(err => {
        console.error(err);
        res.status(400).json(err);
    });
};

/**
 * Saves the resume with the given id
 * @param req
 * @param res
 */
ResumesController.save = (req, res) => {
    ResumeRepository.save(req.params.id, req.body).then(resume => {
        res.json(resume);
    }).catch(err => {
        console.error(err);
        res.status(400).json(err);
    });
};

module.exports = ResumesController;